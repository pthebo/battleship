Improving yourself. From design to implementation using tests, refactoring,
git; oop; python, cpp.

Classic game Battleship with two players and the ability to add new features.

Rules (https://en.wikipedia.org/wiki/Battleship_(game))
As an environment setting, we can change:
- desk size;
- boat types;
- number of boats;
- autofill border around the boat.

The first player is randomly selected.
