import random


def inputNewPosition(set_of_values):
	if (len(set_of_values) == 0):
		return None

	x = -0.1
	while(x not in set_of_values):
		x = int(input("Enter new position: "))

	set_of_values.discard(x)
	return x


def generateNewPosition(set_of_values):
	if (len(set_of_values) == 0):
		return None

	random.seed()
	x = random.choice(tuple(set_of_values))
	set_of_values.discard(x)

	return x


def newPosition(set_of_values, is_generate=False):
	if (is_generate == False):
		return inputNewPosition(set_of_values)
	
	return generateNewPosition(set_of_values)


def inputTheTry(remaining_set_of_values):
	if (len(remaining_set_of_values) == 0):
		return None

	x = -0.1
	while(x not in remaining_set_of_values):
		x = int(input("Enter new try: "))
	
	remaining_set_of_values.discard(x)
	return x


def generateTheTry(remaining_set_of_values):
	if (len(remaining_set_of_values) == 0):
		return None

	random.seed()
	x = random.choice(tuple(remaining_set_of_values))
	remaining_set_of_values.discard(x)
	
	return x


def theTry(remaining_set_of_values, is_generate=False):
	if (is_generate == False):
		return inputTheTry(remaining_set_of_values)
	
	return generateTheTry(remaining_set_of_values)


def checkTheNumber(setted_value, input_value):
	return setted_value == input_value


def newPlayer(name, is_computer=False):
	player = {'isWon': False,
			'name': name,
			'won_cnt': 0,
			'is_computer': is_computer}

	return player


def resetPlayer(player, initial_set):
	player['isWon'] = False
	player['set'] = set(initial_set)

	buf_set = set(initial_set)
	player['val'] = newPosition(buf_set, player['is_computer'])
	

def screamWinnerName(player1, player2):
	if (player1['isWon'] == True):
		player1['won_cnt'] += 1
		print(player1['name'] + " is winner!")
	else:
		player2['won_cnt'] += 1
		print(player2['name'] + " is winner!")	


def theGame(player1, player2):
	initial_set = {i for i in range(1,11)}
	resetPlayer(player1, initial_set)
	resetPlayer(player2, initial_set)

	while((not player1['isWon']) and (not player2['isWon'])):
		
		player1['isWon'] = (checkTheNumber(player2['val'],
			theTry(player1['set'], player1['is_computer'])))
		if (player1['isWon'] == False):
			player2['isWon'] = (checkTheNumber(player1['val'],
				theTry(player2['set'], player2['is_computer'])))

	screamWinnerName(player1, player2)

if (__name__ == '__main__'):
	p1 = newPlayer("Player 1")
	p2 = newPlayer("Player 2", True)

	games = 0
	while(games <= 0):
		games = int(input("How many games do you want to play? "))
	
	for i in range(games):
		theGame(p1, p2)

	res1 = p1['won_cnt']
	res2 = p2['won_cnt']
	print(f'player 1 won the {res1}')
	print(f'player 2 won the {res2}')
