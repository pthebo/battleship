import sys
sys.path.append('../src/')

import unittest
import logic as lgc

class TestLogic(unittest.TestCase):
	def setUp(self):
		pass
		
	
	def test_generateNewPosition(self):
		pos = lgc.generateNewPosition({})
		self.assertEqual(pos, None)

		set_of_values = {i for i in range(5)}
		pos = lgc.generateNewPosition(set_of_values)
		self.assertTrue(pos not in set_of_values)


	def test_generateTheTry(self):
		self.assertEqual(lgc.generateTheTry({}), None)

		remaining_set_of_values = {1,2,5}
		pos = lgc.generateTheTry(remaining_set_of_values)
		self.assertTrue(pos not in remaining_set_of_values)

		
	def test_checkTheNumber(self):
		self.assertFalse(lgc.checkTheNumber(1, 2))
		self.assertTrue(lgc.checkTheNumber(3,3))


if (__name__ == '__main__'):
	unittest.main()
